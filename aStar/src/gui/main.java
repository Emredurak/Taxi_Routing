package gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import javax.swing.JFrame;

import java.awt.GridLayout;

import javax.swing.JPanel;

import aStar.Coordinat;
import aStar.Flight;
import aStar.Node;
import aStar.ReadExcel;
import aStar.Time;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import aStar.graph;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;

import javax.swing.JTabbedPane;

import java.awt.Insets;

import javax.swing.SwingConstants;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

import javax.swing.border.LineBorder;

public class main {
	
	private JFrame frame;
	private JPanel panel_1;
	JPanel panel;
	JButton buttonAddflight;
	JButton buttonDeleteFlight;
	JButton removeAll;
	final JFileChooser fc = new JFileChooser();
	JLabel label_1,label_3,label_5,label_7,label_9,label_11,label_13,label_15;
	String path = "";
	static aStar.graph g = new aStar.graph();
	double maxWidth;
	double maxHeight;
	static List<Flight> flights = new ArrayList<Flight>();
	List<Flight> tempFlights = new ArrayList<Flight>();
	JList<String> list;
	DefaultListModel<String> listModel = new DefaultListModel<String>();
	DrawLine line = null;
	double Xratio = 1;
	double Yratio = 1;
	List <Node> tempPath = null;
	List <Node> flightPath = null;
	Time totalTimeAStar = new Time();
	Time totalTimeGreedy = new Time();
	Time totalTimeImprovedAStar = new Time();
	JMenuItem mn�tmCreateGraph = new JMenuItem("Read Nodes");
	JMenuItem mn�tmReadEdges = new JMenuItem("Read Edges");
	JMenuItem mn�tmAddFlight = new JMenuItem("Add Flight");
	/**
	 * Launch the application.
	 */
	
	public String excellChooser(){
		File a=fc.getSelectedFile();
		String x=a.getAbsolutePath();
		String path ="";
		String y="";
		for(int i=0;i<x.length();i++){
		y=y+x.charAt(i);
		if(x.charAt(i)=='\\')
			y=y+'\\';
		}
		if (x.substring(x.length()-4).equals("xlsx")){
			
			path=path+y;
			JOptionPane.showMessageDialog(null, "Successful!");
		}
		else{
			path = null;
			JOptionPane.showMessageDialog(null, "L�tfen bir .xlsx dosyas� se�iniz");
		}
		return path;
	}
	
	public Flight findFlight(String flightID){
		Flight result = new Flight();
		for(int i=0;i< flights.size(); i++){
			if(flights.get(i).getFlightNumber().equalsIgnoreCase(flightID)){
				result = flights.get(i);
				break;
			}
		}
		return result;
	}
	
	public void populateFlightDetails(Flight flight){
		label_3.setText(flight.getAircraftType().toUpperCase());
		label_7.setText(""+flight.getFlightType().getSpeed()+" m/s");
		label_1.setText(flight.getFlightNumber().toUpperCase());
		label_5.setText(flight.getFlightType().getType().toUpperCase());
		label_9.setText(flight.getStartingPoint().getID().toUpperCase());
		label_13.setText(flight.getStartingTime());
		label_11.setText(flight.getTerminalPoint().getID().toUpperCase());
		if(flight.getDestinationTimeImprovedAstar() != null){
			
		label_15.setText(flight.getElapsedImprovedAStar().toString().toUpperCase());
		}
	}
	
	public boolean isExistFlight(String str){
		boolean isExist = false;
    	for(int i =0 ;i< flights.size();i++){
    		if(flights.get(i).getFlightNumber().equalsIgnoreCase(str))
    			isExist = true;
    	}
    	return isExist;
	}
	
	public void printGraph(graph graph){
		Coordinat coor = graph.findMaxXY();
		maxWidth = coor.getX();
		maxHeight = coor.getY();
		
		Xratio = 1100/maxWidth;
		Yratio = 600/maxHeight;
				
		line = new DrawLine(graph,Xratio,Yratio);
		line.setBounds(10, 10, 1200, 650);
		line.setLayout(null);
		line.setBorder(null);
		
		panel.add(line);
		panel.setVisible(false);
		panel.setVisible(true);
	}
	
	public void hataOlustu(String hata_mesaji){
		JOptionPane.showMessageDialog(null, hata_mesaji);
		
	}
	
	public void reloadList(){
		listModel = new DefaultListModel<>();
		for(Flight f:flights){
			listModel.addElement(f.getFlightNumber());
		}
		list = new JList<String>();
		list.setModel(listModel);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				 try {
				 	main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

	
	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	
	
	private void initialize() {
		frame = new JFrame();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double width = screenSize.getWidth();
		double height = screenSize.getHeight();
		frame.setBounds(100, 100, (int)width, (int)height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
				
		panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(null);
			
				
		
		buttonAddflight = new JButton("AddFlight");
		buttonAddflight.setEnabled(false);
		buttonAddflight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Flight flight1 ;
			
				JTextField flightID = new JTextField(5);
			    JTextField aircraftType = new JTextField(5);
			    JTextField flightType = new JTextField(5);
			    JTextField startNodeID = new JTextField(5);
			    JTextField terminalNodeID = new JTextField(5);
			    JTextField startingTime = new JTextField(5);
			    final ButtonGroup group = new ButtonGroup();
			    
			    JPanel myPanel = new JPanel();
			    myPanel.setLayout(new GridBagLayout());
			    GridBagConstraints c = new GridBagConstraints();
			    
			    c.gridx = 0;
			    c.gridy = 0;
			    myPanel.add(new JLabel("Flight ID:"),c);
			    c.gridx = 1;
			    c.gridy = 0;
			    myPanel.add(flightID,c);
			    
			    c.gridx = 0;
			    c.gridy = 1;
			    myPanel.add(new JLabel("Aircraft Type:"),c);
			    c.gridx = 1;
			    c.gridy = 1;
			    myPanel.add(aircraftType,c);
			    
			    c.gridx = 0;
			    c.gridy = 2;
			    myPanel.add(new JLabel("Flight Type:"),c);
			    c.gridx = 1;
			    c.gridy = 2;
			    myPanel.add(flightType,c);
			    

			    c.gridx = 0;
			    c.gridy = 3;
			    myPanel.add(new JLabel("Is Flight Arrival?"),c);
			    c.gridx = 1;
			    c.gridy = 3;
			    AbstractButton abstract1 = new JRadioButton("Yes");
		        myPanel.add(abstract1,c);
			    c.gridx = 2;
			    c.gridy = 3;
		        group.add(abstract1);
		        AbstractButton abstract2 = new JRadioButton("No");
		        myPanel.add(abstract2,c);
		        group.add(abstract2);
		        group.setSelected(abstract1.getModel(),true);
		        
			    c.gridx = 0;
			    c.gridy = 4;
			    myPanel.add(new JLabel("Start Point:"),c);
			    c.gridx = 1;
			    c.gridy = 4;
			    myPanel.add(startNodeID,c);

			    c.gridx = 0;
			    c.gridy = 5;
			    myPanel.add(new JLabel("Terminal Point:"),c);
			    c.gridx = 1;
			    c.gridy = 5;
			    myPanel.add(terminalNodeID,c);

			    c.gridx = 0;
			    c.gridy = 6;
			    myPanel.add(new JLabel("Starting Time:"),c);
			    c.gridx = 1;
			    c.gridy = 6;
			    myPanel.add(startingTime,c);

			    boolean selected = true;
			    if(abstract1.isSelected()){
			    	selected = true;
			    }else{
			    	selected = false;
			    }
			    
			    int result = JOptionPane.showConfirmDialog(null, myPanel,
			        "Please Enter Flight Details", JOptionPane.OK_CANCEL_OPTION);
			    if (result == JOptionPane.OK_OPTION) {
			     	flight1 = new Flight(flightID.getText(),
				    			aircraftType.getText(), 
				    			flightType.getText(), 
				    			selected, 
				    			g.getNode(startNodeID.getText()), 
				    			g.getNode(terminalNodeID.getText()), 
				    			startingTime.getText());
				    	flights.add(flight1);
				    	
				    	
				    	listModel.addElement(flightID.getText().toUpperCase());
				    	list.setModel(listModel);
				    	System.out.println(flight1);
			    	}
					
			    }
			    
		});
		buttonAddflight.setBounds(1200, 10, 139, 23);
		panel.add(buttonAddflight);
		
		buttonDeleteFlight = new JButton("Delete Flight");
		buttonDeleteFlight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = list.getSelectedIndex();
				Flight flight = findFlight(list.getSelectedValue().toString());
				if(flight!=null){
					flights.remove(flight);
				}
				list.clearSelection();
				listModel.remove(index);
				
				list.setModel(listModel);
				if(listModel.isEmpty()){
					removeAll.setEnabled(false);
					buttonDeleteFlight.setEnabled(false);
				}
			}
		});
		buttonDeleteFlight.setEnabled(false);
		buttonDeleteFlight.setBounds(1200, 41, 139, 23);
		panel.add(buttonDeleteFlight);
		
		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for(int i=0 ;i<flights.size();i++){
					flights.get(i).calculatePath(g);
					for(Node n: flights.get(i).explored){
						boolean exp = false;
						for(int j=0;j<=i;j++){
							if(flights.get(j).getPath().contains(n)){
								exp = true;
								j = i+1;
							}
						}
						if(!exp){
							n.clearSchedule();
						}
							
					}
					System.out.println(flights.get(i));
					
		            System.out.println("Improved A* Path for " + flights.get(i).getFlightNumber() +": " + flights.get(i).getPath());
		            totalTimeImprovedAStar = Time.sum(totalTimeImprovedAStar, flights.get(i).getElapsedImprovedAStar());
		            g.resetGraph();
		            
		            flights.get(i).calculatePathWithGreedy(g);
		            System.out.println("Greedy Path for: " + flights.get(i).getFlightNumber() +": " + flights.get(i).pathGreedy);
		            totalTimeGreedy = Time.sum(totalTimeGreedy, flights.get(i).getElapsedGreedy());
		            g.resetGraph();
		            
		            flights.get(i).calculatePathWithAStar(g);
		            System.out.println("A* Path: " + flights.get(i).getFlightNumber() +": " + flights.get(i).pathAstar);
		            totalTimeAStar = Time.sum(totalTimeAStar, flights.get(i).getElapsedAStar());
		            g.resetGraph();
		            System.out.println("\n\n");
		            
				}
				for(Node n:g.nodes){
	            	n.clearSchedule();
	            }
				
				System.out.println("TOTAL T�ME ELAPSED FOR IMPROVED ASTAR: "+totalTimeImprovedAStar);
				System.out.println("TOTAL T�ME ELAPSED FOR ASTAR: "+totalTimeAStar);
				System.out.println("TOTAL T�ME ELAPSED FOR GREEDY: "+totalTimeGreedy);
				Time gA = Time.minus(totalTimeGreedy, totalTimeAStar);
				Time iaA = Time.minus(totalTimeImprovedAStar, totalTimeAStar);
				Time gIA = Time.minus(totalTimeGreedy, totalTimeImprovedAStar);
				System.out.println("GREEDY - ASTAR = "+ gA +"\t%"+
						(gA.timeAsSeconds()/totalTimeAStar.timeAsSeconds())*100);
				System.out.println("IMPROVED ASTAR - ASTAR = "+ iaA +"\t%"+
						(iaA.timeAsSeconds()/totalTimeAStar.timeAsSeconds())*100);
				System.out.println("GREEDY - IMPROVED ASTAR = "+ gIA +"\t%"+
						(gIA.timeAsSeconds()/totalTimeImprovedAStar.timeAsSeconds())*100);
				hataOlustu("Taxi routes created");
				list.clearSelection();
			}
		});
		btnCalculate.setBounds(1200, 663, 139, 23);
		panel.add(btnCalculate);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(1200, 103, 140, 296);
		panel.add(tabbedPane);
		
		
		
		
		list = new JList<String>(listModel);
		list.setSelectedIndex(0);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.addListSelectionListener(	new ListSelectionListener(){
				@Override
				public void valueChanged(ListSelectionEvent arg0) {
					
					line.getGraphics();
					
					if(tempPath!=null){
						line.paintRoute(line.getGraphics(), tempPath,Color.BLACK, 0);
					}
					
					Flight flight = new Flight();
					
					if(!list.isSelectionEmpty()){
						
						flight = findFlight(list.getSelectedValue().toString());
						flightPath = flight.getPath();
						
					
						if(flight == null || line == null){
							hataOlustu("Flight couldn't find");
						}else {
							
							if(flightPath != null){
								populateFlightDetails(flight);
								line.paintRoute(line.getGraphics(), flightPath, new Color(30,188,80), 0);
								line.paintStartNode(line.getGraphics(), flight.getTerminalPoint());
								
							}else{
								hataOlustu("Path couldn't find! \n Check whether you calculate path.");
								
							}
						}
						tempPath = flightPath;
					}
					if(flights.isEmpty()){
						buttonDeleteFlight.setEnabled(false); 
					   	}else{
					   		buttonDeleteFlight.setEnabled(true);
					   		removeAll.setEnabled(true);
					   	}
					}
				}
					
					);
		
		tabbedPane.addTab("Flights", null,new JScrollPane(list), null);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_2.setBounds(1200, 428, 140, 224);
		panel.add(panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);
		
		JLabel label = new JLabel("ID:");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.insets = new Insets(5, 5, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		panel_2.add(label, gbc_label);
		
		label_1 = new JLabel("");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 0;
		panel_2.add(label_1, gbc_label_1);
		
		JLabel label_2 = new JLabel("Aircraft:");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.anchor = GridBagConstraints.WEST;
		gbc_label_2.insets = new Insets(0, 5, 5, 5);
		gbc_label_2.gridx = 0;
		gbc_label_2.gridy = 1;
		panel_2.add(label_2, gbc_label_2);
		
		label_3 = new JLabel("");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.anchor = GridBagConstraints.WEST;
		gbc_label_3.insets = new Insets(0, 0, 5, 0);
		gbc_label_3.gridx = 2;
		gbc_label_3.gridy = 1;
		panel_2.add(label_3, gbc_label_3);
		
		JLabel label_4 = new JLabel("Type:");
		GridBagConstraints gbc_label_4 = new GridBagConstraints();
		gbc_label_4.anchor = GridBagConstraints.WEST;
		gbc_label_4.insets = new Insets(0, 5, 5, 5);
		gbc_label_4.gridx = 0;
		gbc_label_4.gridy = 2;
		panel_2.add(label_4, gbc_label_4);
		
		label_5 = new JLabel("");
		GridBagConstraints gbc_label_5 = new GridBagConstraints();
		gbc_label_5.anchor = GridBagConstraints.WEST;
		gbc_label_5.insets = new Insets(0, 0, 5, 0);
		gbc_label_5.gridx = 2;
		gbc_label_5.gridy = 2;
		panel_2.add(label_5, gbc_label_5);
		
		JLabel label_6 = new JLabel("Speed:");
		GridBagConstraints gbc_label_6 = new GridBagConstraints();
		gbc_label_6.anchor = GridBagConstraints.WEST;
		gbc_label_6.insets = new Insets(0, 5, 5, 5);
		gbc_label_6.gridx = 0;
		gbc_label_6.gridy = 3;
		panel_2.add(label_6, gbc_label_6);
		
		label_7 = new JLabel("");
		GridBagConstraints gbc_label_7 = new GridBagConstraints();
		gbc_label_7.anchor = GridBagConstraints.WEST;
		gbc_label_7.insets = new Insets(0, 0, 5, 0);
		gbc_label_7.gridx = 2;
		gbc_label_7.gridy = 3;
		panel_2.add(label_7, gbc_label_7);
		
		JLabel label_8 = new JLabel("Start:");
		GridBagConstraints gbc_label_8 = new GridBagConstraints();
		gbc_label_8.anchor = GridBagConstraints.WEST;
		gbc_label_8.insets = new Insets(0, 5, 5, 5);
		gbc_label_8.gridx = 0;
		gbc_label_8.gridy = 4;
		panel_2.add(label_8, gbc_label_8);
		
		label_9 = new JLabel("");
		GridBagConstraints gbc_label_9 = new GridBagConstraints();
		gbc_label_9.anchor = GridBagConstraints.WEST;
		gbc_label_9.insets = new Insets(0, 0, 5, 0);
		gbc_label_9.gridx = 2;
		gbc_label_9.gridy = 4;
		panel_2.add(label_9, gbc_label_9);
		
		JLabel label_10 = new JLabel("End:");
		GridBagConstraints gbc_label_10 = new GridBagConstraints();
		gbc_label_10.anchor = GridBagConstraints.WEST;
		gbc_label_10.insets = new Insets(0, 5, 5, 5);
		gbc_label_10.gridx = 0;
		gbc_label_10.gridy = 5;
		panel_2.add(label_10, gbc_label_10);
		
		label_11 = new JLabel("");
		GridBagConstraints gbc_label_11 = new GridBagConstraints();
		gbc_label_11.anchor = GridBagConstraints.WEST;
		gbc_label_11.insets = new Insets(0, 0, 5, 0);
		gbc_label_11.gridx = 2;
		gbc_label_11.gridy = 5;
		panel_2.add(label_11, gbc_label_11);
		
		JLabel label_12 = new JLabel("Time:");
		GridBagConstraints gbc_label_12 = new GridBagConstraints();
		gbc_label_12.anchor = GridBagConstraints.WEST;
		gbc_label_12.insets = new Insets(0, 5, 5, 5);
		gbc_label_12.gridx = 0;
		gbc_label_12.gridy = 6;
		panel_2.add(label_12, gbc_label_12);
		
		label_13 = new JLabel("");
		GridBagConstraints gbc_label_13 = new GridBagConstraints();
		gbc_label_13.anchor = GridBagConstraints.WEST;
		gbc_label_13.insets = new Insets(0, 0, 5, 0);
		gbc_label_13.gridx = 2;
		gbc_label_13.gridy = 6;
		panel_2.add(label_13, gbc_label_13);
		
		JLabel lblTimeElapsed = new JLabel("Time Elapsed:");
		GridBagConstraints gbc_lblTimeElapsed = new GridBagConstraints();
		gbc_lblTimeElapsed.insets = new Insets(0, 5, 5, 5);
		gbc_lblTimeElapsed.gridx = 0;
		gbc_lblTimeElapsed.gridy = 7;
		panel_2.add(lblTimeElapsed, gbc_lblTimeElapsed);
		
		label_15 = new JLabel("");
		GridBagConstraints gbc_label_15 = new GridBagConstraints();
		gbc_label_15.insets = new Insets(0, 0, 5, 0);
		gbc_label_15.gridx = 2;
		gbc_label_15.gridy = 7;
		panel_2.add(label_15, gbc_label_15);
		
		JLabel lblFlightDetails = new JLabel("Flight Details");
		lblFlightDetails.setHorizontalAlignment(SwingConstants.CENTER);
		lblFlightDetails.setBounds(1200, 410, 139, 23);
		panel.add(lblFlightDetails);
		
		removeAll = new JButton("Delete All Flights");
		removeAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				list.clearSelection();
				listModel.removeAllElements();
				list.setModel(listModel);
				if(!flights.isEmpty()){
					flights.removeAll(flights);
				}
				if(listModel.isEmpty()){
					removeAll.setEnabled(false);
					buttonDeleteFlight.setEnabled(false);
				}
			}
		});
		removeAll.setEnabled(false);
		removeAll.setBounds(1200, 75, 139, 23);
		panel.add(removeAll);
		
		
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		
		mn�tmCreateGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int returnVal = fc.showOpenDialog(mn�tmCreateGraph);
				String path = excellChooser();
				if(path != null){
					g = ReadExcel.readNodes(path);
				}
				mn�tmReadEdges.setEnabled(true);
			}
		});
		
		
		mnFile.add(mn�tmCreateGraph);
		
		mn�tmReadEdges.setEnabled(false);
		mn�tmReadEdges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int returnVal = fc.showOpenDialog(mn�tmCreateGraph);
				String path = excellChooser();
				if(path != null){
					ReadExcel.createLinks(g , path);
					printGraph(g);

					
				}
				buttonAddflight.setEnabled(true);
				mn�tmAddFlight.setEnabled(true);
								
			}
		});
		mnFile.add(mn�tmReadEdges);
				
		mn�tmAddFlight.setEnabled(false);
		mn�tmAddFlight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int returnVal = fc.showOpenDialog(mn�tmCreateGraph);
				String path = excellChooser();
				if(path != null){
					tempFlights = ReadExcel.readFlights(path, g);
				}
				
				
				if(tempFlights!=null){
				for(int i = 0; i<tempFlights.size()-1;i++){
					listModel.addElement(tempFlights.get(i).getFlightNumber().toUpperCase());
					flights.add(tempFlights.get(i));
				}
				}
				tempFlights.removeAll(tempFlights);
				list.setModel(listModel);
				if(!flights.isEmpty()){
					removeAll.setEnabled(true);
					buttonDeleteFlight.setEnabled(true);
				}
				
			}
		});
		mnFile.add(mn�tmAddFlight);

		
	}
}
