package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.sql.Time;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;

import aStar.Coordinat;
import aStar.Edge;
import aStar.Node;
import aStar.graph;

public class DrawLine extends JPanel {

	public List<Node> sources;
	public List<Node> targets;
	public graph graph = null;
	double xRatio;
	double yRatio;
	
    public DrawLine() {
        setBorder(BorderFactory.createLineBorder(Color.black));
    }
    
    public DrawLine(graph g,double scaleX, double scaleY){
    	this.xRatio = scaleX;
    	this.yRatio = scaleY;
    	this.graph = g;
    	setBorder(BorderFactory.createLineBorder(Color.black));
    }

    public Dimension getPreferredSize() {
        return new Dimension(250,200);
    }

    public void paintRoute(Graphics gr, List<Node> nodes,Color color, double time) {
    	
    	Graphics2D g = (Graphics2D) gr;
        g.setStroke(new BasicStroke(2));
    	int sourceX = 0;
        int sourceY = 0;
        int targetX = 0;
        int targetY = 0;
        int count = 0;
        while(count<nodes.size()-1){
    		sourceX = (int)(nodes.get(count).getCoordinats().getX()*xRatio);
        	sourceY = (int)(nodes.get(count).getCoordinats().getY()*yRatio);
        	targetX = (int)(nodes.get(count+1).getCoordinats().getX()*xRatio);
        	targetY = (int)(nodes.get(count+1).getCoordinats().getY()*yRatio);
        	
        	g.setColor(color);
        	g.drawLine(sourceX + 15, sourceY + 15, targetX + 15, targetY + 15);
        	g.drawOval(sourceX, sourceY, 30, 30);
        	g.fillOval(sourceX, sourceY, 30, 30);
        	g.setColor(Color.WHITE);
        	g.drawString(nodes.get(count).getID(), sourceX + 5, sourceY + 20);
        	count++;
        	try {
				Thread.sleep((long) time);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
           }
        sourceX = (int)(nodes.get(count).getCoordinats().getX()*xRatio);
    	sourceY = (int)(nodes.get(count).getCoordinats().getY()*yRatio);
    	g.setColor(color);
    	g.drawOval(sourceX, sourceY, 30, 30);
    	g.fillOval(sourceX, sourceY, 30, 30);
    	g.setColor(Color.WHITE);
    	g.drawString(nodes.get(count).getID(), sourceX + 5, sourceY + 20);
	}
    
    public void update(Graphics2D g,Node start, Node end)
    {
    	g.setColor(Color.BLUE);
    	g.setStroke(new BasicStroke(2));
    	g.drawLine((int)(start.getCoordinats().getX()*xRatio) + 15, (int)(start.getCoordinats().getY()*yRatio) + 15,
    			(int)(end.getCoordinats().getX()*xRatio) + 15, (int)(end.getCoordinats().getY()*yRatio) + 15);
    	
    }
    
    public void paintStartNode(Graphics gr, Node node) {
    	Graphics2D g = (Graphics2D) gr;
    	g.setStroke(new BasicStroke(2));
    	Color color = new Color(2, 216, 37);
    	g.setColor(color);
    	int sourceX = (int)(node.getCoordinats().getX()*xRatio);
    	int sourceY = (int)(node.getCoordinats().getY()*yRatio);
    	g.drawOval(sourceX, sourceY, 30, 30);
    	g.fillOval(sourceX, sourceY, 30, 30);
    	g.setColor(Color.WHITE);
    	g.drawString(node.getID(), sourceX + 5, sourceY + 20);
    }
    public void paintComponent(Graphics gr) {
        super.paintComponent(gr);  
        Graphics2D g = (Graphics2D) gr;
        g.setStroke(new BasicStroke(2));
        int sourceX = 0;
        int sourceY = 0;
        int targetX = 0;
        int targetY = 0;
        g.setColor(Color.BLACK);
        for(Node n : graph.nodes){
        	sourceX = (int)(n.getCoordinats().getX()*xRatio);
        	sourceY = (int)(n.getCoordinats().getY()*yRatio);
        	
        	for(Edge e : n.adjacencies){
        		targetX = (int)(e.getTarget().getCoordinats().getX()*xRatio);
            	targetY = (int)(e.getTarget().getCoordinats().getY()*yRatio);
	        	g.drawLine(sourceX + 15, sourceY + 15, targetX + 15, targetY + 15);
	        	g.drawOval(sourceX, sourceY, 30, 30);
	        	g.fillOval(sourceX, sourceY, 30, 30);
	        	
        	}
        }

    	g.setColor(Color.WHITE);
        for(Node n:graph.nodes){
        	g.drawString(n.getID(), (int)(n.getCoordinats().getX()*xRatio)+5, (int)(n.getCoordinats().getY()*yRatio)+20);
        }
    
    }  
      
}