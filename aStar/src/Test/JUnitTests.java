package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import aStar.Time;

public class JUnitTests {

	@Test
	public void timeToString() {
		Time time = new Time("8.20.30");
		String sonuc = time.toString();
		assertEquals("08:20:30", sonuc);
		
	}
	@Test
	public void timeToString2() {
		Time time = new Time("10.3.0");
		String sonuc = time.toString();
		assertEquals("10:03:00", sonuc);
	}
	@Test
	public void timeSum() {
		Time t1 = new Time("10.3.10");
		Time t2 = new Time("20.59:50");
		Time result = Time.sum(t1, t2);
		assertEquals("07:03:00", result.toString());
	}
}
