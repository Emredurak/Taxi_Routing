package aStar;

public class Time {
	
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
	
	public Time(String time){
		int [] timeArray = new int[3];
		String str="";
		if(!(time.contains(".") || time.contains(":") || time.contains("/"))){
			System.out.println("Time format error");
		}else{
			int j = 0;
			for(int i = 0; i<time.length(); i++){
				
				if (time.charAt(i) == '.' || time.charAt(i) == ':' || time.charAt(i) == '/' ){
					timeArray[j] = Integer.parseInt(str);
					str = "";
					if(j<2){
						j++;
					}else {
						System.out.println("Time format should be \"hh:mm:ss\"");
						break;
					}
				}else{
					str = str + time.charAt(i);
				}
			}
			timeArray[2] = Integer.parseInt(str);
		}
	
	this.setHour(timeArray[0]);
	this.setMinute(timeArray[1]);
	this.setSecond(timeArray[2]);
	}

	Time(int h, int m, int s){
		this.hour = h;
		this.minute = m;
		this.second = s;
	}
	
	public Time(){}
	
	@Override
	public String toString() {
		String str="";
		String substr ="";
		if(this.hour < 10){
			substr = "0";
			str = substr + this.hour + ":";
			substr = "";
		}else
			str = str + this.hour + ":";
		if(this.minute < 10){
			substr = "0";
			str = str + substr + this.minute + ":";
			substr = "";
		}else
			str = str + this.minute + ":";
		if(this.second < 10){
			substr = "0";
			str = str + substr + this.second;
		}else
			str = str + this.second;
		return str;
	}
	
	public static Time sum(Time t1, Time t2){
		double sumSeconds = 0;
		int h = 0;
		int m = 0;
		int s = 0;
		sumSeconds = t1.getSecond() + t1.getMinute()*60 + t1.getHour()*3600
				+ t2.getSecond() + t2.getMinute()*60 + t2.getHour()*3600;
		
		h = (int)sumSeconds/3600;
		sumSeconds = sumSeconds - 3600*h;
		m = (int)sumSeconds/60;
		sumSeconds = sumSeconds - 60*m;
		s = (int)sumSeconds;
		Time sum = new Time(h%24,m,s);
		return sum;
	}
	public static Time sum(Time t1, double t2){
		double sumSeconds = 0;
		int h = 0;
		int m = 0;
		int s = 0;
		sumSeconds = t1.getSecond() + t1.getMinute()*60 + t1.getHour()*3600 + t2;
		
		h = (int)sumSeconds/3600;
		sumSeconds = sumSeconds - 3600*h;
		m = (int)sumSeconds/60;
		sumSeconds = sumSeconds - 60*m;
		s = (int)sumSeconds;
		Time sum = new Time(h,m,s);
		return sum;
	}
	
	public static Time minus(Time t1, Time t2){
		double sumSeconds = 0;
		sumSeconds = t1.getSecond() + t1.getMinute()*60 + t1.getHour()*3600
				- (t2.getSecond() + t2.getMinute()*60 + t2.getHour()*3600);
		Time time = new Time();
		time.hour = (int)sumSeconds/3600;
		sumSeconds = sumSeconds - 3600*time.hour;
		time.minute = (int)sumSeconds/60;
		sumSeconds = sumSeconds - 60*time.minute;
		time.second = (int)sumSeconds;
		return time;
	}
	
	public static double minus2(Time t1, Time t2){
		double sumSeconds = 0;
		sumSeconds = t1.getSecond() + t1.getMinute()*60 + t1.getHour()*3600
				- (t2.getSecond() + t2.getMinute()*60 + t2.getHour()*3600);
		
		return sumSeconds;
	}
	
	public double timeAsSeconds(){
		double seconds =  second + minute*60 + hour*3600;
		return seconds;
	}
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}
}
