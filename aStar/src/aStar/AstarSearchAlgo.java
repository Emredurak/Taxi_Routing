package aStar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class AstarSearchAlgo{
	public static List<Node> printPath(Node target){
        List<Node> path = new ArrayList<Node>();

		for(Node node = target; node!=null; node = node.getParent()){
			
		    path.add(node);
		}
		
		Collections.reverse(path);
		
		return path;
	}
	
	public static void AstarSearch(Node source, Node goal,int speed){
		Time startingTime = source.getTime();
        Set<Node> explored = new HashSet<Node>();
        
        PriorityQueue<Node> queue = new PriorityQueue<Node>(80, 
                new Comparator<Node>(){
                         //override compare method
         public int compare(Node i, Node j){
            if(i.getFScores() > j.getFScores()){
                return 1;
            }

            else if (i.getFScores() < j.getFScores()){
                return -1;
            }

            else{
                return 0;
            }
         }

                }
                );

        //cost from start
        source.setGScores(0);

        queue.add(source);

        boolean found = false;
        
        while((!queue.isEmpty())&&(!found)){
        		
                //the node in having the lowest f_score ID
                Node current = queue.poll();

                explored.add(current);
                
                //goal found
                if(current.getID().equals(goal.getID())){
                        found = true;
                        break;
                }

                //check every child of current node
                for(Edge e : current.getAdjacencies()){
                        Node child = e.getTarget();
                        double cost = e.getCost();
                        double temp_gScores = current.getGScores() + cost;
                        double temp_fScores = temp_gScores + child.getHScores();
                    
                        /*if child node has been evaluated and 
                        the newer f_score is higher, skip*/
                        
                        if((explored.contains(child)) && 
                                (temp_fScores >= child.getFScores())){
                                continue;
                        }

                        /*else if child node is not in queue or 
                        newer f_score is lower*/
                        
                        else if((!queue.contains(child)) || 
                                (temp_fScores < child.getFScores())){

	                        	double timePassed = temp_gScores/speed;
								child.setTime(Time.sum(startingTime, timePassed));
                                child.setParent(current);
                                child.setGScores(temp_gScores);
                                child.setFScores(temp_fScores);

                                if(queue.contains(child)){
                                        queue.remove(child);
                                }

                                queue.add(child);
                                
                        }

                }

        }
        
}
}