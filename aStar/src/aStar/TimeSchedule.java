package aStar;

public class TimeSchedule {
	private String flightID;
	private Time time;
	private FlightType flightType;
	
	public TimeSchedule() {}
	public TimeSchedule(String flightID, Time time, FlightType flightType){
		this.flightID = flightID;
		this.time = time;
		this.flightType = flightType;
	}
	public String getFlightID() {
		return flightID;
	}
	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public FlightType getFlightType() {
		return flightType;
	}
	public void setFlightType(FlightType flightType) {
		this.flightType = flightType;
	}
	
	
	
}
