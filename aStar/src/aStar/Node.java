package aStar;

import java.util.ArrayList;
import java.util.List;

public class Node {
	private String ID;
	private double gScores;
    private double hScores;
    private double fScores = 0;
    public List <Edge> adjacencies;
    private Node parent;
    private Coordinat coordinats = new Coordinat();
    private Time time = new Time();
    private List<TimeSchedule> nodeSchedule = new ArrayList<TimeSchedule>();
    public Node(){}
    public Node(String id, double hValue){
            ID = id;
            hScores = hValue;
    }
    public Node(String id){
    	this.ID = id;
    }
    public Node(String id,Coordinat coordinat){
    	this.ID = id;
    	this.coordinats = coordinat;
    }
    
    @Override
    public String toString() {
    	String str = ID + " "+ time;
    	return str;
    }
    
    public String getID(){
    	return ID;
    }
    public void setID(String iD) {
		ID = iD;
	}
    public Node findShortestEdge(){
    	Node result = null;
    	if(adjacencies!=null){
	    	result = adjacencies.get(0).getTarget();
	    	double min = adjacencies.get(0).getCost();
	    	for(Edge e : adjacencies){
	    		if(e.getCost()<min){
	    			min = e.getCost();
	    			result = e.getTarget();
	    		}
	    	}
	    	}
    	return result;
    }
    public void addToSchedule(TimeSchedule timeSchedule){
    	nodeSchedule.add(timeSchedule);
    }
    public void clearSchedule(){
    	nodeSchedule.clear();
    }
    public boolean checkNodeSchedule(TimeSchedule timeSchedule){
    	boolean isConflictDetected = false;
    	double safetyInterval = 0;
    	for(TimeSchedule ts: nodeSchedule){
    		
    		double difference = Time.minus2(ts.getTime(), timeSchedule.getTime());
    		if(difference < 0){
    			safetyInterval = getSafetyInterval(ts.getFlightType().getSpeed());
    		}
    		else{
    			safetyInterval = getSafetyInterval(timeSchedule.getFlightType().getSpeed());
    		}
    		if(Math.abs(difference) < safetyInterval && !timeSchedule.getFlightID().equalsIgnoreCase(ts.getFlightID())){
    			isConflictDetected = true;
    			break;
    		}
    	}
    	return isConflictDetected;
    }
    
    public double getSafetyInterval(int speed){
    	double safetyInterval = 0;
    	
		if(speed == 5){
			safetyInterval = 20;
		} else if(speed == 6){
			safetyInterval = 33.3;
		}else if (speed == 7){
			safetyInterval = 42.9;
		}
		
		return safetyInterval;
    }
    
    public void setGScores(double gScore){
    	gScores = gScore;
    }
    public double getGScores(){
    	return gScores;
    }
    public void setHScores(double h){
    	this.hScores = h;
    }
    public double getHScores(){
    	return hScores;
    }
    public double getFScores(){
    	return fScores;
    }
    public void setFScores(double fScore){
    	fScores = fScore;
    }

	public Coordinat getCoordinats() {
		return coordinats;
	}

	public void setCoordinats(Coordinat coordinats) {
		this.coordinats = coordinats;
	}
	public List <Edge> getAdjacencies() {
		return adjacencies;
	}
	public void setAdjacencies(List <Edge> adjacencies) {
		this.adjacencies = adjacencies;
	}
	public Node getParent() {
		return parent;
	}
	public void setParent(Node parent) {
		this.parent = parent;
	}
	public Time getTime() {
		return time;
	}
	public void setTime(Time time) {
		this.time = time;
	}
	public List<TimeSchedule> getNodeSchedule() {
		return nodeSchedule;
	}
	public void setNodeSchedule(List<TimeSchedule> nodeSchedule) {
		this.nodeSchedule = nodeSchedule;
	}
	
}
