package aStar;

public class FlightType {
	private String type;
	private int speed;
	
	public FlightType() {}
	public FlightType(String type){
		if(type.startsWith("h") || type.startsWith("H")){
			this.type = "Heavy";
			this.speed = 7;
		}else if(type.startsWith("M") || type.startsWith("m")){
				this.type = "Medium";
				this.speed = 6;
			}else if(type.startsWith("l") || type.startsWith("L")){
				this.type = "Light";
				this.speed = 5;
		}else {
			this.type = "Not state";
			System.out.println("Fliight type didn't mentioned");		
	
		}
	}
	public FlightType(int speed){
		if(speed == 5 || speed == 6 ||speed == 7)
			this.speed = speed;
		else
			this.speed = 1;
		
			switch (this.speed){
			case 5: 
				type = "Light";
				break;
			case 6: 
				type = "Medium";
				break;
			case 7 : 
				type = "Heavy";
				break;
				default :type = "Not State";
		}
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	
}
