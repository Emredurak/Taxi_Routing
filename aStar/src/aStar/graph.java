package aStar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class graph {
	
	public void calculateHScores(Node target){
		for( Node n : this.nodes){
        	n.setHScores(Edge.calculateDistance(n.getCoordinats(), target.getCoordinats()));
        }
	}
	
	public Coordinat findMaxXY(){
		Coordinat coor;
		double maxY = 0;
		double maxX = 0;
		for(Node n:nodes){
			if (n.getCoordinats().getX() > maxX){
				maxX = n.getCoordinats().getX();
			}
			if(n.getCoordinats().getY() > maxY){
				maxY = n.getCoordinats().getY();
			}
		}
		coor = new Coordinat(maxX, maxY);
		return coor;
	}
	
	public void resetGraph(){
		for(Node n:nodes){
			n.setGScores(0);
			n.setFScores(0);
			n.setParent(null);
			n.setTime(null);
		}
	}
	
	public void addNode(Node node){
		nodes.add(node);
	}
	
	public Node getNode(String nodeID){
		Node result = null;
		for(Node n : nodes){
			if(n.getID().equalsIgnoreCase(nodeID)){
				result = n;
				break;
			}
		}
		return result;
	}
	
	public int findNode(String NodeID){
		int nodeNumber = 0;
		for(int i = 0; i<nodes.size(); i++){
			if(nodes.get(i).getID().equalsIgnoreCase(NodeID)){
				nodeNumber = i;
				break;
			}
		}
		return nodeNumber;
	}

	public List<Node> nodes = new ArrayList<Node>();
	
}
