package aStar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.bag.SynchronizedSortedBag;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel {
	public static final String pathNode = ".\\Nodes.xlsx";
	public static final String pathEdge = ".\\Edges.xlsx";
	
    

    
    public static graph readNodes(String path){
    	Workbook workbook;
    	graph graph = new graph();
    	
    	Coordinat coordinat = new Coordinat();
		try {
			workbook = WorkbookFactory.create(new File(path));
			Sheet sheet = workbook.getSheetAt(0);
			
			DataFormatter dataFormatter = new DataFormatter();
			for (Row row: sheet) {
				String id = "";
				double x = 0;
				double y = 0;
				int count = 0;
	            for(Cell cell: row) {
	                String cellValue = dataFormatter.formatCellValue(cell);
	                switch (count){
	                case 0 : 
	                	id =(cellValue);
	                	break;
	                case 1:
	                	x = (Double.parseDouble(cellValue));
	                	break;
	                case 2:
	                	y = (Double.parseDouble(cellValue));
	                	break;
	                }
	                count++;
	                
	            }
	            
	            graph.nodes.add(new Node(id,new Coordinat(x, y)));
	            
	        }
		} catch (EncryptedDocumentException | InvalidFormatException
				| IOException e) {
			e.printStackTrace();
		}
		
		return graph;
    }
    
    
   
    public static void createLinks(graph graph,	String pathEdge){
    	Workbook workbook;
    	
		try {
			workbook = WorkbookFactory.create(new File(pathEdge));
			Sheet sheet = workbook.getSheetAt(0);
			
			DataFormatter dataFormatter = new DataFormatter();
			int index = 0;
			for (Row row: sheet) {
				List <Edge> edges = new ArrayList<Edge>();
				Node n = graph.nodes.get(index);
	            for(Cell cell: row) {
	                String cellValue = dataFormatter.formatCellValue(cell);
	                Node target = new Node();
	                for(int i = 0; i<graph.nodes.size(); i++){
	                	if(graph.nodes.get(i).getID().equalsIgnoreCase(cellValue)){
	                		target = graph.nodes.get(i);
	                		i=graph.nodes.size();
	                	}
	                }
	                if(cellValue != null && !cellValue.equals(""))
	                edges.add(new Edge(n,target));
	            }
	            n.setAdjacencies(edges);
	            graph.nodes.set(index, n);
	            index++;
	        }
		} catch (EncryptedDocumentException | InvalidFormatException
				| IOException e) {
			e.printStackTrace();
		}
    }
    public static List<Flight> readFlights(String path,graph g){
    	Workbook workbook;
    	List <Flight> flights = new ArrayList<Flight>();
    	
    	
		try {
			workbook = WorkbookFactory.create(new File(path));
			Sheet sheet = workbook.getSheetAt(0);
			
			DataFormatter dataFormatter = new DataFormatter();
			for (Row row: sheet) {
				Flight flight = new Flight();
				
				int count = 0;
	            for(Cell cell: row) {
	                String cellValue = dataFormatter.formatCellValue(cell);
	                
	                switch (count){
	                case 0 : 
	                	flight.setFlightNumber(cellValue);
	                	break;
	                case 1:
	                	flight.setAircraftType(cellValue);
	                	break;
	                case 2:
	                	flight.setFlightType(new FlightType(cellValue));
	                	break;
	                case 3:
	                	if(cellValue.equalsIgnoreCase("a")){
	                		flight.setArrival(true);
	                	}else{
	                		flight.setArrival(false);
	                	}
	                	break;
	                case 4:
	                	flight.setStartingPoint(g.getNode(cellValue));
	                	break;
	                case 5:
	                	flight.setTerminalPoint(g.getNode(cellValue));
	                	break;
	                case 6:
	                	flight.setStartingTime(cellValue);
	                	break;
	                
	                }	
	                count++;
	                
	            }
	            
	            flights.add(flight);
	            
	        }
		} catch (EncryptedDocumentException | InvalidFormatException
				| IOException e) {
			e.printStackTrace();
		}
		
		return flights;
    }
}
