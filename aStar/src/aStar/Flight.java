package aStar;

import java.util.List;
import java.util.Set;

public class Flight {

	private String flightNumber;				// uniqe flight id 
	private Node startingPoint;				// taxi starting point
	private Node terminalPoint;				// taxi ending point
	private String aircraftType;				// b-777, b747, a180 etc.
	private FlightType flightType;					// Heavy, medium or light
	private boolean isArrival;					// arrival flights have priority
	private List<Node> path = null;
	public List<Node> pathGreedy;
	public List<Node> pathAstar;
	private Time startingTime;
	public Set<Node> explored;
	private Time DestinationTimeAstar;
	private Time DestinationTimeImprovedAstar;
	private Time DestinationTimeGreedy;
	private Time elapsedAStar;
	private Time elapsedGreedy;
	private Time elapsedImprovedAStar;
	public Flight(){
		
	}
	public Flight(String flightNumber, String aircraftType, String flightType, boolean isArrival, Node startingPoint, Node terminalPoint, String startingTime){
		this.flightNumber = flightNumber;
		this.aircraftType = aircraftType;
		this.flightType = new FlightType(flightType);
		this.isArrival = isArrival;
		this.startingPoint = startingPoint;
		this.terminalPoint = terminalPoint;
		this.startingTime = new Time(startingTime);
	}
	public Flight(String flightNumber, String aircraftType, int speed, boolean isArrival, Node startingPoint, Node terminalPoint, String startingTime){
		this.flightNumber = flightNumber;
		this.aircraftType = aircraftType;
		this.flightType = new FlightType(speed);
		this.isArrival = isArrival;
		this.startingPoint = startingPoint;
		this.terminalPoint = terminalPoint;
		this.startingTime = new Time(startingTime);
	}
	public void calculatePath(graph g){
		startingPoint.setTime(startingTime);
		g.calculateHScores(terminalPoint);
		explored = improvedAstar.AstarSearch(startingPoint, terminalPoint, flightType.getSpeed(), startingTime, flightNumber, flightType);
		setPath(improvedAstar.printPath(terminalPoint));
		DestinationTimeImprovedAstar = calculateTime(path);
		elapsedImprovedAStar = Time.minus(DestinationTimeImprovedAstar, startingTime);
		clearPath();
		
	}
	public Time getDestinationTimeAstar() {
		return DestinationTimeAstar;
	}
	public void setDestinationTimeAstar(Time destinationTimeAstar) {
		DestinationTimeAstar = destinationTimeAstar;
	}
	public Time getDestinationTimeImprovedAstar() {
		return DestinationTimeImprovedAstar;
	}
	public void setDestinationTimeImprovedAstar(Time destinationTimeImprovedAstar) {
		DestinationTimeImprovedAstar = destinationTimeImprovedAstar;
	}
	public Time getDestinationTimeAstarGreedy() {
		return DestinationTimeGreedy;
	}
	public void setDestinationTimeAstarGreedy(Time destinationTimeAstarGreedy) {
		DestinationTimeGreedy = destinationTimeAstarGreedy;
	}
	public void setStartingTime(Time startingTime) {
		this.startingTime = startingTime;
	}
	public void calculatePathWithAStar(graph g){
		startingPoint.setTime(startingTime);
		g.calculateHScores(terminalPoint);
		AstarSearchAlgo.AstarSearch(startingPoint, terminalPoint, flightType.getSpeed());
		pathAstar = (AstarSearchAlgo.printPath(terminalPoint));
		DestinationTimeAstar = calculateTime(pathAstar);
		elapsedAStar = Time.minus(DestinationTimeAstar, startingTime);
	}
	public void calculatePathWithGreedy(graph g){
		startingPoint.setTime(startingTime);
		Greedy.greedyUCS(startingPoint, terminalPoint);
		pathGreedy = (Greedy.printPath(terminalPoint));
		DestinationTimeGreedy = calculateTime(pathGreedy);
		elapsedGreedy = Time.minus(DestinationTimeGreedy, startingTime);
	}
	public void clearPath(){
		for(Node n : explored){
			if(!path.contains(n)){
				n.clearSchedule();
			}
		}
	}
	public Time calculateTime(List<Node> path){
		Node start = startingPoint;
		Time endtime = startingTime;
		for(Node end:path){
			double x = Edge.calculateDistance(start.getCoordinats(), end.getCoordinats());
			double timePassed = x/flightType.getSpeed();
			endtime = (Time.sum(endtime, timePassed));
			end.setTime(endtime);
			start = end;
		}
		return endtime;
	}
	@Override
	public String toString() {
		String str = "Flight ID:" + flightNumber + 
				"\t Start Time:" + startingTime + 
				"\t Start Point:" + startingPoint + 
				"\t Terminal Point:" + terminalPoint + 
				"\t Aircraft Type:" + aircraftType + 
				"\t Speed:" + flightType.getSpeed() + " m/s"; 
		
		return str;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Node getStartingPoint() {
		return startingPoint;
	}
	public void setStartingPoint(Node startingPoint) {
		this.startingPoint = startingPoint;
	}
	public Node getTerminalPoint() {
		return terminalPoint;
	}
	public void setTerminalPoint(Node terminalPoint) {
		this.terminalPoint = terminalPoint;
	}
	public String getAircraftType() {
		return aircraftType;
	}
	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}
	public FlightType getFlightType() {
		return flightType;
	}
	public void setFlightType(FlightType flightType) {
		this.flightType = flightType;
	}
	public boolean isArrival() {
		return isArrival;
	}
	public void setArrival(boolean isArrival) {
		this.isArrival = isArrival;
	}
	public List<Node> getPath() {
		return path;
	}
	public void setPath(List<Node> path) {
		this.path = path;
	}
	public String getStartingTime() {
		return startingTime.toString();
	}
	public void setStartingTime(String startingTime) {
		this.startingTime = new Time(startingTime);
	}
	public List<Node> getPathGreedy() {
		return pathGreedy;
	}
	public void setPathGreedy(List<Node> pathGreedy) {
		this.pathGreedy = pathGreedy;
	}
	public List<Node> getPathAstar() {
		return pathAstar;
	}
	public void setPathAstar(List<Node> pathAstar) {
		this.pathAstar = pathAstar;
	}
	public Set<Node> getExplored() {
		return explored;
	}
	public void setExplored(Set<Node> explored) {
		this.explored = explored;
	}
	public Time getDestinationTimeGreedy() {
		return DestinationTimeGreedy;
	}
	public void setDestinationTimeGreedy(Time destinationTimeGreedy) {
		DestinationTimeGreedy = destinationTimeGreedy;
	}
	public Time getElapsedAStar() {
		return elapsedAStar;
	}
	public void setElapsedAStar(Time elapsedAStar) {
		this.elapsedAStar = elapsedAStar;
	}
	public Time getElapsedGreedy() {
		return elapsedGreedy;
	}
	public void setElapsedGreedy(Time elapsedGreedy) {
		this.elapsedGreedy = elapsedGreedy;
	}
	public Time getElapsedImprovedAStar() {
		return elapsedImprovedAStar;
	}
	public void setElapsedImprovedAStar(Time elapsedImprovedAStar) {
		this.elapsedImprovedAStar = elapsedImprovedAStar;
	}
	
	
	

}
