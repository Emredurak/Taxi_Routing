package aStar;

public class Coordinat {

	private double x;
	private double y;
	
	public Coordinat(){
		this.x = 0;
		this.y = 0;
	}
	Coordinat(double x, double y){
		this.x = x;
		this.y = y;
	}
		@Override
		public String toString() {
			String str ="" + this.x + "," + this.y;
			return str;
		}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
}
