package aStar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class Greedy {
	
	public static List<Node> printPath(Node target){
        List<Node> path = new ArrayList<Node>();

		for(Node node = target; node!=null; node = node.getParent()){
			
		    path.add(node);
		}
		
		Collections.reverse(path);
		
		return path;
	}
	
	
	public static void greedyUCS(Node source, Node goal){
	source.setGScores(0);
    PriorityQueue<Node> queue = new PriorityQueue<Node>(20, 
        new Comparator<Node>(){

            //override compare method
            public int compare(Node i, Node j){
                if(i.getGScores() > j.getGScores()){
                    return 1;
                }

                else if (i.getGScores() < j.getGScores()){
                    return -1;
                }

                else{
                    return 0;
                }
            }
        }

    );

    queue.add(source);
    Set<Node> explored = new HashSet<Node>();
    boolean found = false;

    //while frontier is not empty
    do{

        Node current = queue.poll();
        explored.add(current);

        if(current.getID().equals(goal.getID())){
            found = true;


        }

        for(Edge e: current.adjacencies){
            Node child = e.getTarget();
            double cost = e.getCost();
            child.setGScores(current.getGScores() + cost);



            if(!explored.contains(child) && !queue.contains(child)){

                child.setParent(current);
                queue.add(child);

            }
            else if((queue.contains(child))&&(child.getGScores() > current.getGScores())){
            	child.setParent(current);

                // the next two calls decrease the key of the node in the queue
                queue.remove(child);
                queue.add(child);
            }


        }
    }while(!queue.isEmpty());

}
}