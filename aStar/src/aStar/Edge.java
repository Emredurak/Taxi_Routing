package aStar;


public class Edge {
	private final double cost;
    private final Node target;
    
    public Edge(Node source,Node targetNode){
            target = targetNode;
            cost = calculateDistance(source.getCoordinats(),target.getCoordinats());
    }
    public Edge(Node target,double val){
    	this.cost = val;
    	this.target = target;
    }
    
    public static double calculateDistance(Coordinat c1,Coordinat c2){
		double distance = 0;
		distance = Math.sqrt(Math.pow((c1.getX()-c2.getX()),2)+
				Math.pow((c1.getY()-c2.getY()),2));
		
    	return distance;
    }
	public double getCost() {
		return cost;
	}
	public Node getTarget(){
		return target;
	}
	@Override
	public String toString() {
		String str = "";
		str = target.getID();
		
		return str;
	}
}
